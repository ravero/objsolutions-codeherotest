using System;
using System.Globalization;
using Xamarin.Forms;

namespace CodeHero.ValueConverters
{
    [ValueConversion(typeof(string), typeof(string))]
    public class UppercaseConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string == false)
            {
                return default(string);
            }

            var input = (string)value;
            return input.ToUpper();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException("Can't convert back to original input value.");
        }
    }
}