﻿using System;
using System.Collections.Generic;
using System.Linq;
using CodeHero.Models;
using CodeHero.ViewModels;
using Xamarin.Forms;

namespace CodeHero.Pages
{
    public partial class MainPage : ContentPage
    {
        MainViewModel viewModel;
        bool isFirstLoaded;

        public MainPage()
        {
            InitializeComponent();
            BindingContext = viewModel = new MainViewModel(Navigation);
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            // Evita que o conteúdo seja recarregado sempre que a página for re-apresentada
            if (!isFirstLoaded)
            {
                await viewModel.RefreshAsync();
                isFirstLoaded = true;
            }
        }

        async void CollectionView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // TODO: ideal seria converter para tratar seleção através de MVVM.
            var selection = e.CurrentSelection.FirstOrDefault();
            if (selection != null &&
                selection is Character character &&
                sender is CollectionView collectionView)
            {
                await Navigation.PushAsync(new CharacterDetailPage(character));
                collectionView.SelectedItem = null;
            }
        }
    }
}
