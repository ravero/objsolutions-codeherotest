﻿using System;
using System.Collections.Generic;
using CodeHero.Models;
using CodeHero.ViewModels;
using Xamarin.Forms;

namespace CodeHero.Pages
{
    public partial class CharacterDetailPage : ContentPage
    {
        CharacterDetailViewModel viewModel;

        public CharacterDetailPage(Character character)
        {
            InitializeComponent();
            BindingContext = viewModel = new CharacterDetailViewModel(character);
        }
    }
}
