﻿using System;
using System.Security.Cryptography;
using System.Text;
using CodeHero.Models;
using Flurl;

namespace CodeHero.Utils
{
    /// <summary>
    /// Extensões do Flurl para a construção de chamadas de API da Marvel.
    /// </summary>
    public static class MarvelApiExtensions
    {
        /// <summary>
        /// Adiciona os parâmetros de autenticação necessários para realizar uma chamada ao API da Marvel.
        /// Conforme documentação: https://developer.marvel.com/documentation/authorization
        /// </summary>
        /// <param name="publicKey">Chave pública da conta.</param>
        /// <param name="privateKey">Chave privada da conta.</param>
        /// <returns></returns>
        public static Url AddMarvelAuthenticationParameters(this Url url, string publicKey, string privateKey)
        {
            var timestamp = DateTime.Now.ToFileTime();
            var contentToHash = $"{timestamp}{privateKey}{publicKey}";
            var hash = GetMD5Hash(contentToHash);

            return url
                .SetQueryParam("apikey", publicKey)
                .SetQueryParam("ts", timestamp)
                .SetQueryParam("hash", hash);
        }

        /// <summary>
        /// Adiciona os parâmetros de paginação para uma chamada de API da Marvel.
        /// </summary>
        /// <param name="pageData">Um objeto PageData contendo os dados de paginação que devem ser aplicados.</param>
        /// <returns></returns>
        public static Url AddMarvelPageParameters(this Url url, PageData pageData) => url
            .SetQueryParam("limit", pageData.PageSize)
            .SetQueryParam("offset", pageData.RecordOffset);

        /// <summary>
        /// Calcula o Hash MD5 de uma string.
        /// </summary>
        /// <param name="content">Conteúdo para o qual o hash deve ser calculado.</param>
        /// <returns>Uma string com o hash MD5 calculado para o conteúdo.</returns>
        static string GetMD5Hash(string content)
        {
            using var md5Hash = MD5.Create();
            var contentBytes = Encoding.UTF8.GetBytes(content);
            var hash = md5Hash.ComputeHash(contentBytes);
            var builder = new StringBuilder();

            foreach (var b in hash)
            {
                builder.Append(b.ToString("x2"));
            }

            return builder.ToString();
        }
    }
}
