﻿using System;

namespace CodeHero.Models
{
    /// <summary>
    /// Representa o objeto raiz retornado pelas chamadas de API da Marvel.
    /// Contém informações básicas a respeito da chamada de API executada.
    /// </summary>
    /// <typeparam name="TData">O tipo de dado do conteúdo retornado pela chamada de API.</typeparam>
    public class ApiDataWrapper<TData>
    {
        public int Code { get; set; }

        public string Status { get; set; }

        public string Copyright { get; set; }

        public string AttributionText { get; set; }

        public string AttributionHTML { get; set; }

        public TData Data { get; set; }

        public string Etag { get; set; }
    }
}
