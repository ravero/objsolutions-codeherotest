﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace CodeHero.Models
{
    /// <summary>
    /// Representa um personagem da Marvel
    /// </summary>
    public class Character
    {
        /// <summary>
        /// Identificador numérico do API.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nome do Personagem.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Descrição.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Informações para obtenção da miniatura representando a imagem do personagem.
        /// </summary>
        public ImageDescriptor Thumbnail { get; set; }

        /// <summary>
        /// URL's relacionadas a esse personagem.
        /// </summary>
        public IEnumerable<UrlDescriptor> Urls { get; set; }

        /// <summary>
        /// URL para imagem do personagem no formato quadrado tamanho "XLarge".
        /// </summary>
        public string ThumbnailUrl => $"{Thumbnail.Path}/standard_xlarge.{Thumbnail.Extension}";
    }
}
