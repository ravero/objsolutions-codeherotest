﻿using System;

namespace CodeHero.Models
{
    public class UrlDescriptor
    {
        public string Type { get; set; }

        public string Url { get; set; }
    }
}
