﻿using System;

namespace CodeHero.Models
{
    /// <summary>
    /// Representa as informações de paginação.
    /// Essa estrutura é imutável e inclui diversos métodos que produzem um novo registro trasformado.
    /// Esse modelo é inspirado em programação funcional e F#.
    /// </summary>
    public struct PageData
    {
        /// <summary>
        /// Número da página atualmente representada.
        /// </summary>
        public int CurrentPage { get; }

        /// <summary>
        /// Quantidade de registros que podem ser contidos em uma página.
        /// </summary>
        public int PageSize { get; }

        /// <summary>
        /// Quantidade total de registros na Query a qual essa paginação se refere.
        /// </summary>
        public int TotalRecords { get; }

        /// <summary>
        /// Quantidade total de páginas continas na Query a qual essa paginação se refere.
        /// </summary>
        public int TotalPages => TotalRecords > 0
            ? (int)Math.Ceiling(TotalRecords / (double)PageSize)
            : 1;

        /// <summary>
        /// Retorna a quantidade de registros que devem ser pulados para início da página atual
        /// na fonte de dados, com base no número da página atual e no tamanho de uma página.
        /// </summary>
        public int RecordOffset => CalculateOffset(CurrentPage, PageSize);

        public PageData(int currentPage, int pageSize, int totalRecords = 0)
        {
            // Protege contra a passagem de um número de página inválido.
            if (currentPage < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(currentPage), "The current page can't be below 1. Paging starts at 1.");
            }

            CurrentPage = currentPage;
            PageSize = pageSize;
            TotalRecords = totalRecords;
        }

        /// <summary>
        /// Retorna se é possível retroceder uma página.
        /// </summary>
        /// <returns></returns>
        public bool CanGoPrevious() => CurrentPage > 1;

        /// <summary>
        /// Retorna se é possível avançcar uma página.
        /// </summary>
        /// <returns></returns>
        public bool CanGoNext()
        {
            /// Se o número total de registros da Query não foi passado não temos como determinar se é possível avançar
            if (TotalRecords == 0)
            {
                return true;
            }

            var nextPage = CurrentPage + 1;
            var offset = CalculateOffset(nextPage, PageSize);
            return offset <= TotalRecords;            
        }

        /// <summary>
        /// Retorna uma nova instância desse objeto apontando para a página especificada.
        /// </summary>
        /// <param name="pageNumber">Número da página para avançar até.</param>
        /// <returns>Uma instância do objeto na página especificada.</returns>
        public PageData ToPage(int pageNumber) => new PageData(pageNumber, PageSize, TotalRecords);

        public PageData ToNextPage() => new PageData(CurrentPage + 1, PageSize, TotalRecords);

        public PageData ToPreviousPage() => new PageData(CurrentPage - 1, PageSize, TotalRecords);

        public PageData WithTotal(int totalRecords) => new PageData(CurrentPage, PageSize, totalRecords);

        static int CalculateOffset(int currentPage, int pageSize) => (currentPage - 1) * pageSize;
    }
}
