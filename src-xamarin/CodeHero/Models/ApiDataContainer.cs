﻿using System;
using System.Collections.Generic;

namespace CodeHero.Models
{
    /// <summary>
    /// Um objeto representando os dados retornados por uma chamada de API da Marvel.
    /// Contém informações básicas a respeito da quantidade de objetos no API e retornados na chamada
    ///     e os resultados dessa chamada.
    /// </summary>
    /// <typeparam name="TResult">Tipo de dado dos elementos da lista de resultados.</typeparam>
    public class ApiDataContainer<TResult>
    {
        public int Offset { get; set; }

        public int Limit { get; set; }

        public int Total { get; set; }

        public int Count { get; set; }

        public IEnumerable<TResult> Results { get; set; }
    }
}
