﻿using System;

namespace CodeHero.Models
{
    /// <summary>
    /// Representa o caminho para uma imagem na API da Marvel.
    /// Conforme a documentação em https://developer.marvel.com/documentation/images, a URL para uma imagem
    ///     é representada de maneira módular para que se possa obter as imagens nos diversos
    ///     tamahos disponíveis.
    /// </summary>
    public class ImageDescriptor
    {
        /// <summary>
        /// Caminho (URL) da imagem.
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// Extensão para ser usada ao obter a imagem.
        /// </summary>
        public string Extension { get; set; }
    }
}
