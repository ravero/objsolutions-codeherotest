﻿using System;

namespace CodeHero
{
    /// <summary>
    /// Contém declarações de constantes e configurações compartilhadas pelo projeto.
    /// </summary>
    public static class Constants
    {
        public const string MarvelApiBaseUrl = "https://gateway.marvel.com/";

        public const string MarvelApiPublicKey = "4a63c2aaf6892777523953a8c3cae71b";

        public const string MarvelApiPrivateKey = "7fc78b6769c8c342279d4d7ab1378f12d776867d";
    }
}
