﻿using System;
using System.Linq;
using System.Windows.Input;
using CodeHero.Models;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace CodeHero.ViewModels
{
    public class CharacterDetailViewModel : BaseViewModel
    {
        readonly Character character;

        string _name;
        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        string _description;
        public string Description
        {
            get => _description;
            set => SetProperty(ref _description, value);
        }

        string _thumbnailUrl;
        public string ThumbnailUrl
        {
            get => _thumbnailUrl;
            set => SetProperty(ref _thumbnailUrl, value);
        }

        public ICommand DetailsCommand => new Command(async () =>
        {
            var detailUrl = character.Urls.FirstOrDefault(u => u.Type == "detail");
            if (detailUrl != null)
            {
                await Browser.OpenAsync(detailUrl.Url, BrowserLaunchMode.SystemPreferred);
            }
        });

        public ICommand WikiCommand => new Command(async () =>
        {
            var detailUrl = character.Urls.FirstOrDefault(u => u.Type == "wiki");
            if (detailUrl != null)
            {
                await Browser.OpenAsync(detailUrl.Url, BrowserLaunchMode.SystemPreferred);
            }
        });

        public CharacterDetailViewModel(Character character)
        {
            this.character = character;
            LoadModel(character);
        }

        void LoadModel(Character character)
        {
            ThumbnailUrl = character.ThumbnailUrl;
            Name = character.Name;
            Description = string.IsNullOrWhiteSpace(character.Description)
                ? "No Description was provided for this character."
                : character.Description;
        }
    }
}
