﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace CodeHero.ViewModels
{
    /// <summary>
    /// Classe base para objetos do tipo ViewModel do MVVM.
    /// Contém recursos comuns aos View Models.
    /// </summary>
    public abstract class BaseViewModel : INotifyPropertyChanged
    {
        protected readonly INavigation Navigation;

        public BaseViewModel()
        {
        }

        public BaseViewModel(INavigation navigation)
        {
            this.Navigation = navigation;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = null)
        {
            if (Equals(storage, value))
                return false;

            storage = value;
            OnPropertyChanged(propertyName);
            return true;
        }

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
