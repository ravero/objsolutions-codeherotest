﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using CodeHero.Models;
using CodeHero.Services;
using Xamarin.Forms;

namespace CodeHero.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        const int PageSize = 4;

        IMarvelService marvelService;
        PageData currentPage = DefaultPage();

        private bool _isBusy;
        public bool IsBusy
        {
            get => _isBusy;
            set => SetProperty(ref _isBusy, value);
        }

        private string _name;
        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        string _pageDescription = "Não há registros";
        public string PageDescription
        {
            get => _pageDescription;
            set => SetProperty(ref _pageDescription, value);
        }

        public ObservableCollection<Character> Items { get; set; } = new ObservableCollection<Character>();

        public ICommand GoToPreviousPageCommand => new Command(async () =>
        {
            if (currentPage.CanGoPrevious())
            {
                currentPage = currentPage.ToPreviousPage();
                await RefreshAsync();
            }
        });

        public ICommand GoToNextPageCommand => new Command(async () =>
        {
            if (currentPage.CanGoNext())
            {
                currentPage = currentPage.ToNextPage();
                await RefreshAsync();
            }
        });

        public ICommand SearchCommand => new Command(async () =>
        {
            await RefreshAsync(resetPage: true);
        });

        public MainViewModel(INavigation navigation) : base(navigation)
        {
            marvelService = new MarvelService();
        }

        internal async Task RefreshAsync(bool resetPage = false)
        {
            IsBusy = true;

            if (resetPage)
            {
                currentPage = DefaultPage();
            }

            var result = await marvelService.GetCharactersAsync(currentPage, Name);
            if (result == null)
            {
                currentPage = DefaultPage();
                PageDescription = "Não há registros";
                return;
            }

            currentPage = currentPage.WithTotal(result.Total);

            Items.Clear();
            result.Results
                .ToList()
                .ForEach(c => Items.Add(c));

            PageDescription = $"{currentPage.CurrentPage} de {currentPage.TotalPages}";

            IsBusy = false;
        }

        static PageData DefaultPage() => new PageData(1, PageSize);
    }
}
