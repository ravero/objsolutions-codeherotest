﻿using System;
using System.Threading.Tasks;
using CodeHero.Models;

namespace CodeHero.Services
{
    public interface IMarvelService
    {
        Task<ApiDataContainer<Character>> GetCharactersAsync(PageData pageData, string name = null);
    }
}
