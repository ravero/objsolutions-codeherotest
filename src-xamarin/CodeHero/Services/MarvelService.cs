﻿using System;
using System.Threading.Tasks;
using CodeHero.Models;
using CodeHero.Utils;
using Flurl;
using Flurl.Http;

namespace CodeHero.Services
{
    public class MarvelService : IMarvelService
    {
        public async Task<ApiDataContainer<Character>> GetCharactersAsync(PageData pageData, string name = null)
        {
            try
            {
                var url = Constants.MarvelApiBaseUrl
                    .AppendPathSegments("v1", "public", "characters")
                    .AddMarvelAuthenticationParameters(Constants.MarvelApiPublicKey, Constants.MarvelApiPrivateKey)
                    .AddMarvelPageParameters(pageData);

                if (!string.IsNullOrWhiteSpace(name))
                {
                    url.SetQueryParam("nameStartsWith", name);
                }

                var results = await url
                    .GetAsync()
                    .ReceiveJson<ApiDataWrapper<ApiDataContainer<Character>>>();

                return results.Data;
            }
            catch
            {
                // TODO: Aprimorar o tratamento de erros.
                // Ideal aqui seria retornar um objeto de status da operação.
                return null;
            }
        }        
    }
}
