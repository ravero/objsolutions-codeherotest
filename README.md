# Projeto Code Hero
Esse repositório contém o código fonte do projeto Code Hero, implementação de teste técnico de programação da Objective Solutions por Rafael Veronezi. A versão atual implementa todos os requisitos básicos e o Plus em **Xamarin.Forms**, com o resultado disponível para as plataformas Android e iOS. Procurei seguir as específicações ao máximo, mas tomei algumas liberdades em alguns ajustes por conta restrito para implementação da solução:

* **Uso da barra de navegação**: a barra de navegação não faz parte da especificação do layout, mas eu decidi usa-la por alguns motivos:
    1. Oferece uma interface familiar para o usuário final.
    2. Fornece uma estrutura de navegação padronizada considerando a inclusão da página de detalhes, mencionada abaixo.
    3. No iOS, calcula automaticamente o espaçameto com o topo da tela que é variável dependendo do dispositivo que esta sendo usado. Os dispositivos a partir do iPhone X tem a parte superior com o notch e os dispositivos anteriores a ele contém a barra de status, portando esse espaçamento varia.
* **Botão Buscar**: no campo de busca inclui um botão "Buscar" que não estava previsto no layout original, que da entender que a busca é feita a medida que o nome do personagem vai sendo digitado. Embora essa seja a experiència otimizada, como não tive tempo de implementar a lógica para cancelamento de uma Task de busca do ativo entre as digitações dos caracteres no campo de texto, optei usar esse botão de busca a fim de bloquear a UI enquanto a busca estava sendo realizada (`IsBusy`).
* **Botões de Paginação**: o layout proposto preve a inclusão de botões de paginação. Embora eu tenha iniciado a implementação desses, optei por remove-las da versão para entrega pois não consegui testar o suficiente para garantir seu funcionamento correto. Além disso, em meus testes quando o número da página começava a ficar com 2 ou 3 dígitos, ele quebrava totalmente o layout já que o número não cabia mais dentro do layout circular proposto. Por esses motivos substitui essa barra de botões com um indicador do número da página atual.
* **Detalhes do personagem**: A tela de detalhes do plus não foi especificada, então tomei a liberdade de incluir um layout simples contendo apenas a foto, nome e descrição do personagem em questão. Outros dados poderiam ter sido obtidos através do API, mas optei por não inclui-los por questão de tempo, afim de priorizar outras partes que considero mais importantes na implementação.

## Arquitetura

Para Xamarin.Forms foi usado o modelo MVVM. Toda a lógica do aplicativo se encontra no projeto `CodeHero` que é uma biblioteca .NET Standard 2.1. Foi escolhida a versão mais recente do .NET Standard afim de se poder utilizar o C# 8.0, já suportado nas versões recentes do Xamarin.iOS e Xamarin.Android.

As seguintes bibliotecas adicionais foram utilizadas:

* `Flurl.Http`: usada para facilitar as chamadas HTTP, é uma biblioteca .NET Standard que permite a construção de URL's e chamados HTTP no modelo de Fluent API, facilitando a construção e leitura desse tipo de interação.
* `FFImageLoading`: usado para obter as imagens de Thumbnail que aparecem na listagem.
* `Xamarin.Essentials`: usado para abrir links externos de Detalhes e Wiki na página de detalhes do personagem.

O projeto utiliza um modelo de serviços com o acesso aos API's da Marvel declarado através de interfaces. Ele foi concebido para uso de de injeção de dependencias e modularização para testes unitários, porém isso não foi implementado na versão atual do código.

## Detalhes e Omissões

Alguns detalhes que deixei de implementar por questões de tempo, mas que foram considerados durate o desenvolvimento e entram como melhorias para uma nova versão:

* **Ajustes visuais no Android**: alguns ajustes visuais ainda precisam ser feitos para versão Android do aplicativo, na barra de navegação, nos botões para eliminar o background, entre outros detalhes. O desenvolvimento foi feito primariamente testando no simulador do iOS, por esse motivo esses detalhes do Android ficaram para o final e não houve tempo para ajustá-los, mas todos são relativemente simples de serem aplicados usando os recursos do Xamarin.Forms.
* **_Placeholder_ na carga das imagens das fotos dos personagens**: durante a carga das images de thumbnail dos personagens, faltou incluir uma imagem de placeholder (loading). Isso é importante para sinalizar ao usuário final que a imagem esta sendo obtida. Igualmente importante é a inclusão de uma imagem de erro. Ambas funcionalidades são suportadas pelo component FFImageLoader, mas não tive tempo de montar uma imagem para incluir essas funções, então as omiti.
* **Fontes**: nas especificações de layout mencionava o uso da fonte Roboto. Essa informação é ambigua considerando que minha implemetação contemplatava tanto a plataforma iOS e Android, e que a fonte Roboto é padrão do Android, por esse motivo eu mantive a fonte padrão na implementação iOS. A título de exemplo de utilização das fontes eu inclui as fonte [**Marvel Font**](https://www.fontspace.com/ybtfonts/marvel) e [**Heroes Assemble**](https://www.fontspace.com/iconian-fonts/heroes-assemble), usadas na tela de Splash do iOS e na tela de detalhes.

## Ferramentas usadas

O projeto foi todo desenvolvido em Mac, usando o macOS Catalina, Visual Studio for Mac 8.4 e Xcode 11.3.1.